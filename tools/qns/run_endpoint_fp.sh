#!/bin/bash
set -e

# The following variables are available for use:
# - ROLE contains the role of this execution context, client or server
# - SERVER_PARAMS contains user-supplied command line parameters
# - CLIENT_PARAMS contains user-supplied command line parameters

QUICHE_DIR=/quiche
WWW_DIR=/www
DOWNLOAD_DIR=/downloads
QUICHE_CLIENT=quiche-client
QUICHE_SERVER=quiche-server
QUICHE_CLIENT_OPT="--no-verify --dump-responses ${DOWNLOAD_DIR}"
QUICHE_SERVER_OPT_COMMON="--cert examples/cert.crt --key examples/cert.key"
QUICHE_SERVER_OPT="$QUICHE_SERVER_OPT_COMMON --no-retry "
LOG_DIR=/logs
LOG=$LOG_DIR/log.txt

run_quiche_server_tests() {
    $QUICHE_DIR/$QUICHE_SERVER --listen 0.0.0.0:4433 --root $WWW_DIR \
        $SERVER_PARAMS $QUICHE_SERVER_OPT >& $LOG
}

# Create quiche log directory
mkdir -p $LOG_DIR

run_quiche_server_tests
